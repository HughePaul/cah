
// nice rand wrapper
function randomInteger (max) {
	return Math.floor(Math.random()*max);
}

// generate a random text id
function randomString(length) {
	var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	if(!length) { length = 16; }
	var id = '';
	for(var i = 0; i<length; i++) {
		id += letters[randomInteger(letters.length)];
	}
	return id;
}


module.exports = {
	integer: randomInteger,
	string: randomString
};
