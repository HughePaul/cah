
var util = require('util');
var random = require('./random');

// load cardpacks
var cardPacks = require('./packs');
var packFullNames = Object.keys(cardPacks.packs)
	.map(function(packName) {
		return {
			name: packName,
			fullName: cardPacks.packs[packName].name || packName
		};
	});



function BlackCard(cardData){
	if (typeof cardData === 'string') cardData = { t: cardData };
	this.id = random.string();
	this.text = cardData.t;
    this.text = this.text.replace(/_/g, '_____________');
	this.answersRequired = cardData.c || 1;
}
BlackCard.type = 'black';


function WhiteCard(cardData){
	this.id = random.string();
	this.text = cardData;
}
WhiteCard.type = 'white';


// a cardstack holds a stack of cards
function CardStack(cardClass) {
	this.cardClass = cardClass;
	this.bag = [];
}
CardStack.prototype.addPack = function(packName) {
	var pack = cardPacks.packs[packName];
	if (!pack) return;
	var cards = pack[this.cardClass.type];
	if (!cards) return;
	cards.forEach(function(id) {
		var card = cardPacks[this.cardClass.type][id];
		if (this.bag.indexOf(card) >= 0) return;
		this.bag.push(card);
	}.bind(this));
	return this.bag.length;
};
CardStack.prototype.isEmpty = function() {
	return this.bag.length === 0;
};
// when a card is drawn it is actually drawn at random from a bag of cards,
// instead of from the top of a stack
CardStack.prototype.draw = function() {
	if(this.isEmpty()) { return; }
	var pos = random.integer(this.bag.length);
	var cardData = (this.bag.splice(pos,1))[0];
	return new this.cardClass(cardData);
};


WhiteStack = function() {
	CardStack.call(this, WhiteCard);
};
util.inherits(WhiteStack, CardStack);


BlackStack = function() {
	CardStack.call(this, BlackCard);
};
util.inherits(BlackStack, CardStack);


module.exports = {
	WhiteStack: WhiteStack,
	BlackStack: BlackStack,
	packFullNames: packFullNames
};

