
var random = require('./random');

function User(name, socket){
	var user = this;
	user.name = name;
	user.id = random.string();
	user.key = random.string(32);
	user.sockets = [];
	user.spectator = false;
	user.reset();
	User.users[user.id] = user;
	user.queue = [];
	if(socket) {
		user.addSocket(socket);
	}
	user.keepAlive();
}

User.prototype.keepAlive = function(name) {
	var user = this;
	user.pings = 0;
	user.pingTimer = setInterval(function() {
		var stamp = Date.now();
		user.pings++;

		if (user.pings > 6) {
			console.log(user.id, 'PING TIMEOUT');
			user.sockets.forEach(function(socket) {
				socket.disconnect();
			});
			return user.destroy();
		}

		console.log(user.id, 'PING');
		user.sockets.forEach(function(socket) {
			user.pings++;
			socket.emit('pingpong', stamp, function(returnedStamp){
				console.log(user.id, 'PONG', Date.now() - returnedStamp);
				user.pings = 0;
			});
		});
	}, 5000);
};

User.prototype.setName = function(name) {
	var user = this;
	user.name = name;
};
User.prototype.addSocket = function(socket) {
	var user = this;
	if(user.sockets.indexOf(socket) < 0) {
		user.sockets.push(socket);
	}
};
User.prototype.removeSocket = function(socket) {
	var user = this;
	var pos = user.sockets.indexOf(socket);
	if(pos >= 0) {
		user.sockets.splice(pos,1);
	}
	// destroy this user if not in a game and not connected to anything
	if(!user.game && user.sockets.length === 0) {
		user.destroy();
	}
};
User.prototype.emit = function() {
	var user = this;
	var args = [].slice.call(arguments);

	if(!user.sockets.length) {
		// queue event for when the user reconnects
		user.queue.push(JSON.stringify(args));
		return;
	}

	// emit queued events
	if(user.queue.length) {
		user.queue.forEach(function(event){
			user.sockets.forEach(function(socket){
				socket.emit.apply(socket, JSON.parse(event));
			});
		});
		user.queue = [];
	}

	// emit this event
	user.sockets.forEach(function(socket){
		socket.emit.apply(socket, args);
	});
};
User.prototype.reset = function() {
	var user = this;
	user.game = null;
	user.cardsInHand = {};
	user.score = 0;
	user.drops = 0;
};
User.prototype.isCzar = function() {
	var user = this;
	return user.game && user === user.game.currentCzar;
};
// play these cards
User.prototype.play = function(cardIds) {
	var user = this;
	if(!user.game) {
		return;
	}
	// take these cards out of my hand
	var cards = [];
	cardIds.forEach(function(cardId) {
		var card = user.cardsInHand[cardId];
		if(card) {
			delete user.cardsInHand[card.id];
			cards.push(card);
		}
	});
	// and play them to the game
	user.game.play(user, cards);
};
// pickup cards from game
User.prototype.swapCards = function(maxCards) {
	var user = this;
	user.score--;
	user.cardsInHand = {};
	user.emit('cards', user.cardsInHand);
	user.pickupCards(maxCards);
};
User.prototype.pickupCards = function(maxCards) {
	var user = this;
	if(!user.game) {
		return;
	}
	while(
		(user.game.state !== 'gameover') &&
		(Object.keys(user.cardsInHand).length < (maxCards || 10)) &&
		user.game.dealWhiteCard(user)
	) {}
};
// accept a card into my hand
User.prototype.acceptCard = function(card) {
	var user = this;
	if(!user.game) {
		return;
	}
	if(!card) { return; }
	user.cardsInHand[card.id] = card;
	user.emit('card', card);
};
// destroy user
User.prototype.destroy = function() {
	var user = this;
	clearTimeout(user.timer);
	if(user.game){
		user.game.removeUser(user);
	}
	user.cardsInHand = {};
	delete User.users[user.id];
	user.sockets = [];
	clearInterval(user.pingTimer);
};

User.users = {};

module.exports = User;
