
var cards = require('./cards');
var random = require('./random');

var ruleRules = {
	minimumPlayers: { min: 2, max: 5, default: 2 },
	maximumPlayers: { min: 2, max: 10, default: 8 },
	cardsInHand: { min: 5, max: 15, default: 10 },
	playTimeout: { min: 0, max: 120, default: 20 },
	bestTimeout: { min: 0, max: 120, default: 20 },
	maximumDrops: { min: 1, max: 10, default: 4 }
};

function Game(name, rules){
	var game = this;

	// setup rules for this game
	if(typeof rules !== 'object') rules = {};
	game.rules = {
		cardPacks: Array.isArray(rules.cardPacks) && rules.cardPacks || [ 'INTL' ]
	};

	for(var ruleName in ruleRules) {
		var ruleRule = ruleRules[ruleName];
		game.rules[ruleName] =
			Math.min(ruleRule.max,
				Math.max(ruleRule.min,
					parseInt(rules[ruleName], 10) || ruleRule.default));
	}

	game.name = name;
	game.id = random.string();
	Game.games[game.id] = game;
	game.users = [];
	game.reset();
}
Game.prototype.reset = function() {
	var game = this;
	game.currentBlackCard = null;
	game.currentCzar = null;
	// create black and white card stacks
	game.blackStack = new cards.BlackStack();
	game.whiteStack = new cards.WhiteStack();
	game.rules.cardPacks.forEach(function(packName){
		game.blackStack.addPack(packName);
		game.whiteStack.addPack(packName);
	});
	// check we have at least some cards to play with
	if(game.blackStack.isEmpty()) {
		game.blackStack.addPack('INTL');
	}
	if(game.whiteStack.isEmpty()) {
		game.whiteStack.addPack('INTL');
	}
	game.playUsers = [];
	game.playedCards = {};
	game.state = 'created';
	if(game.timer) { clearTimeout(game.timer); }
	game.timer = null;
};
Game.prototype.addUser = function(user, spectator) {
	var game = this;
	if(user.game) {
		return;
	}
	// reset user
	user.reset();
	user.game = game;
	user.spectator = spectator;
	user.drops = 0;

	// announce that user has joined the game
	game.users.push(user);
	var scores = game.getScores();
	game.users.forEach(function(otherUser){
		otherUser.emit('scores', scores);
		otherUser.emit('join',user.id);
	});
	// set game user to be the czar if game is the only user
	if(game.users.length === 1 && !user.spectator) {
		game.currentCzar = user;
	} else {
// dont add to this current play as they might not have cards yet
//		game.playUsers.push(user);
	}
	// update game user with the currect state of the game
	game.refreshUser(user);
};
Game.prototype.removeUser = function(user, onlyDrop) {
	var game = this;
	if(user.game === game) {
		// remove any cards that have been played by this user
		delete game.playedCards[user.id];
		// remove from user user lists
		var pos = game.playUsers.indexOf(user);
		if(pos>=0) { game.playUsers.splice(pos,1); }

		if (onlyDrop) {
			user.spectator = true;
			game.users.forEach(function(otherUser){
				otherUser.emit('spectator', user.id);
			});
		} else {
			pos = game.users.indexOf(user);
			if(pos>=0) { game.users.splice(pos,1); }

			// tell the user they have left
			user.emit('parted', game.id);
			// reset the user
			user.reset();

			// if no more users left then destroy myself
			if(game.users.length === 0) {
				return game.destroy();
			}

			// announce that game user has left the game
			game.users.forEach(function(otherUser){
				otherUser.emit('part', user.id);
			});
		}

		var scores = game.getScores();
		game.users.forEach(function(otherUser){
			otherUser.emit('scores', scores);
		});

		// end game if not enough users
		if(game.state !== 'created' && game.state !== 'gameover' && game.users.length < game.rules.minimumPlayers) {
			console.log(game.id, 'Aborting game, not enough players');
			game.gameOver();
			// if leaving user is currently the Czar then choose another Czar
			if(!game.currentCzar || game.currentCzar === user) {
				game.nextCzar(pos);
			}
		} else if(game.currentCzar === user) {
			// if game user if currently the Czar then choose another Czar
			if(game.state !== 'created' && game.state !== 'gameover') {
				// if the game is progress skip go as we need new logic
				game.nextGo(pos);
			} else {
				// if the game hasn't started then just choose another Czar
				game.nextCzar(pos);
			}
		} else {
			// check if everyone else has played their cards
			game.tryReveal();
		}

	}
};
Game.prototype.refreshUser = function(user) {
	var game = this;
	user.emit('game', game.id, game.name, game.rules);
	user.emit('scores', game.getScores());
	user.emit('czar', game.currentCzar ? game.currentCzar.id : '');
	user.emit('cards', user.cardsInHand);
	user.emit('question',
		game.currentBlackCard,
		game.playUsers.indexOf(user) >= 0 || user.isCzar());
	user.emit('played',
		Object.keys(game.playedCards),
		game.timer && game.state === 'playcards' ? Math.ceil((game.timerExpires - Date.now()) / 1000) : 0);
	if(game.state === 'choosebest') {
		user.emit('reveal',
			game.playedCards,
			game.timer ? Math.ceil((game.timerExpires - Date.now()) / 1000) : 0);
	}
};
Game.prototype.getScores = function() {
	var game = this;
	var scores = {};
	game.users.forEach(function(user){
		scores[user.id] = {
			name: user.name,
			score: user.score,
			spectator: user.spectator
		};
	});
	return scores;
};
Game.prototype.start = function() {
	var game = this;
	if(game.state !== 'created' && game.state !== 'gameover') {
		return false;
	}
	if(game.state !== 'created') {
		game.reset();
	}
	console.log(game.id, 'Game started: '+game.name);
	game.state = 'started';

	game.users.forEach(function(user){
		user.emit('started');
		user.cardsInHand = {};
		user.emit('cards', {});
	});

	game.nextGo();
	return true;
};
Game.prototype.nextGo = function(czarPosition) {
	var game = this;

	// clear timer
	if(game.timer) { clearTimeout(game.timer); game.timer = null; }

	var scores = game.getScores();
	console.log(game.id, 'Next go');
	game.users.forEach(function(user){
		user.emit('scores', scores);
	});

	// if no black cards left then its the end of the game
	if(game.blackStack.isEmpty()) {
		return game.gameOver();
	}

	// make a copy of the current users for this play. new users can join from the next go
	game.playUsers = game.users
		.filter(function(user) { return !user.spectator; })
		.slice(0,game.rules.maximumPlayers);

	// check all users have 10 cards
	game.playUsers.forEach(function(user){
		user.pickupCards(game.rules.cardsInHand);
	});

	// check if the game ended because of no cards left
	if(game.state === 'gameover') {
		return;
	}

	// choose a new czar
	if(game.state !== 'started') {
		game.nextCzar(czarPosition);
	}
	// remove the Czar from the players able to play cards
	czarPosition = game.playUsers.indexOf(game.currentCzar);
	if(czarPosition >= 0) { game.playUsers.splice(czarPosition,1); }
	else {
		console.log(game.id, "Error making play list: ",game.currentCzar,czarPosition);
	}

	// deal a new black card
	game.dealBlackCard();
};
Game.prototype.dealWhiteCard = function(user) {
	var game = this;
	var card = game.whiteStack.draw();
	if(!card){
		game.gameOver();
		return;
	}
	// give this card to the user
	user.acceptCard(card);
	return card;
};
Game.prototype.dealBlackCard = function() {
	var game = this;
	game.currentBlackCard = game.blackStack.draw();
	if(!game.currentBlackCard) {
		game.gameOver();
		return;
	}

	game.users.forEach(function(user){
		user.emit('question', game.currentBlackCard, game.playUsers.indexOf(user) >= 0 || user.isCzar());
	});
	game.playedCards = {};
	game.state = 'playcards';

	if(game.timer) clearTimeout(game.timer);
	game.timer = null;
	game.timerExpires = 0;

	return game.currentBlackCard;
};
Game.prototype.swapCards = function(user) {
	var game = this;
	if(user.score < 1) {
		return 'Not enough points';
	}
	if(game.whiteStack.bag.length < game.rules.cardsInHand) {
		return 'Not enough cards available';
	}
	user.swapCards(game.rules.cardsInHand);
	var scores = game.getScores();
	game.users.forEach(function(otherUser){
		otherUser.emit('swapped', user.id);
		otherUser.emit('scores', scores);
	});
};
Game.prototype.nextCzar = function(czarPosition) {
	var game = this;

	var users = game.playUsers.length ?
		game.playUsers :
		game.users.filter(function(user) { return !user.spectator; });

	// set a new czar (next or random)
	var nextCzarPos =
		(typeof czarPosition === 'number') ?
		czarPosition + 1 :
		users.indexOf(game.currentCzar) + 1;
	if(!game.currentCzar || !nextCzarPos) {
		 nextCzarPos = random.integer(users.length);
	}
	if(nextCzarPos >= users.length) {
		nextCzarPos = 0;
	}
	if(!users[nextCzarPos]) {
		nextCzarPos = 0;
	}
	game.currentCzar = users[nextCzarPos];
	game.users.forEach(function(user){
		user.emit('czar', game.currentCzar.id);
	});
};
Game.prototype.getUserById = function(userId) {
	var game = this;
	for(var i=0; i<game.users.length; i++) {
		if(game.users[i].id === userId) {
			return game.users[i];
		}
	}
};
Game.prototype.answersRequired = function() {
	var game = this;
	if(!game.currentBlackCard) {
		return;
	}
	return game.currentBlackCard.answersRequired;
};
// check if the user is allowed to play in this round
Game.prototype.userCanPlay = function(user) {
	var game = this;
	if(game.users.indexOf(user) < 0) {
		console.log(user.id+' cannot play: User not found in game.');
		return false;
	}
	if(game.playUsers.indexOf(user) < 0) {
		console.log(user.id+' cannot play: User not in current play list.');
		return false;
	}
	if(game.playedCards[user.id]) {
		console.log(user.id+' cannot play: User has already played.');
		return false;
	}
	return true;
};
// play a set of cards from a user
Game.prototype.play = function(user, cards) {
	var game = this;

	if(game.state !== 'playcards'){
		return;
	}

	if(!game.userCanPlay(user)) {
		return;
	}
	if(!cards || !cards.length) {
		return;
	}
	game.playedCards[user.id] = cards;

	// timeout slow players
	if (!game.timer) {
		game.timer = setTimeout(function(){
			if(!game.timer) return;
			game.timer = null;
			if(game.state !== 'playcards') return;
	
			console.log('Play timeout');
			game.playUsers.forEach(function(slowUser){
				if(slowUser === game.currentCzar) return;
				if(game.playedCards[slowUser.id]) return;
				console.log('Dropping', slowUser.id, slowUser.name);
				game.drop(slowUser);
			});
			game.tryReveal();
		}, game.rules.playTimeout * 1000);
		game.timerExpires = Date.now() + game.rules.playTimeout * 1000;
	}

	// announce game user has played
	var playedIds = Object.keys(game.playedCards);
	game.users.forEach(function(user){
		user.emit('played', playedIds, Math.ceil((game.timerExpires - Date.now()) / 1000));
	});

	user.drops = 0;

	game.tryReveal();

	return true;
};
Game.prototype.drop = function(user) {
	var game = this;
	var pos = game.playUsers.indexOf(user);
	if(pos < 0) {
		return;
	}
	game.playUsers.splice(pos,1);
	delete game.playedCards[user.id];

	game.users.forEach(function(otherUser){
		otherUser.emit('dropped', user.id);
	});

	user.drops++;
	if (user.drops >= game.rules.maximumDrops) {
		game.removeUser(user, true);
	}
};
// check if all playing users have played and reveal if they have
Game.prototype.tryReveal = function() {
	var game = this;
	if(game.state !== 'playcards') {
		return;
	}

	if (game.playUsers.length === 0) {
		game.state = 'bestchosen';	
		return setTimeout( function(){
			if (game.state === 'bestchosen') game.nextGo();
		}, 4000);
	}

	// if all users have played then announce game and show cards
	if(Object.keys(game.playedCards).length === game.playUsers.length) {
		game.users.forEach(function(user){
			user.emit('reveal', game.playedCards, game.rules.bestTimeout);
		});
		game.state = 'choosebest';

		// timeout slow czar
		if(game.timer) clearTimeout(game.timer);
		game.timer = setTimeout(function(){
			game.timer = null;
			if(game.state === 'choosebest'){
				game.playUsers.forEach(function(user){
					user.score++;
				});
				var scores = game.getScores();
				game.users.forEach(function(otherUser){
					otherUser.emit('besttimeout');
					otherUser.emit('scores', scores);
				});
				game.state = 'bestchosen';

				game.currentCzar.drops++;

				// after a timeout do the next go
				setTimeout( function(){
					if(	game.state === 'bestchosen') {
						game.nextGo();
					}
				}, 5000);
			}
		}, game.rules.bestTimeout * 1000);
		game.timerExpires = Date.now() + game.rules.bestTimeout;
	}
};
// check if a user is allowed to be a best player in this round
Game.prototype.userCanBeBest = function(user) {
	var game = this;
	if(user === game.currentCzar) {
		return false;
	}
	if(game.users.indexOf(user) < 0) {
		return false;
	}
	if(game.playUsers.indexOf(user) < 0) {
		return false;
	}
	if(!game.playedCards[user.id]) {
		return false;
	}
	return true;
};
// choose the user as the best player in this round
Game.prototype.best = function(user) {
	var game = this;

	if(game.state !== 'choosebest') {
		return false;
	}

	if(!game.userCanBeBest(user)) {
		return false;
	}

	user.score++;
	var scores = game.getScores();
	game.users.forEach(function(otherUser){
		otherUser.emit('best', user.id);
		otherUser.emit('scores', scores);
	});
	game.state = 'bestchosen';

	// after a timeout do the next go
	setTimeout( function(){
		if(	game.state === 'bestchosen') {
			game.nextGo();
		}
	}, 5000);
};
Game.prototype.gameOver = function() {
	var game = this;
	game.currentBlackCard = null;
	var scores = game.getScores();
	game.users.forEach(function(user){
		user.emit('scores',scores);
		user.emit('gameover');
	});
	game.state = 'gameover';
	return false;
};
Game.prototype.chat = function(originatingUser, text) {
	var game = this;
	game.users.forEach(function(user){
		user.emit('chat', originatingUser.id, text);
	});
};
Game.prototype.destroy = function() {
	var game = this;
	game.users.forEach(function(user) {
		if(user.game === game) {
			user.game = null;
		}
	});
	game.users = [];
	game.playUsers = [];
	game.playedCards = {};
	game.currentBlackCard = null;
	game.currentCzar = null;
	game.state = 'gamedestroyed';
	delete Game.games[game.id];
};

Game.games = {};

module.exports = Game;
