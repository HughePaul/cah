
// choose port from env environment or default to port 80
var port = parseInt(process.env.CAH_PORT,10) || 80;


// print an exception nicely
process.on('uncaughtException', function(e){
	console.log(e.stack);
});

var packagejson = require('./package');

// create socket server
var express = require('express');
var app = express();
app.use(express.static('static'));
var server = require('http').createServer(app);
var io = require('socket.io')(server, {
	pingInterval: 10000,
	pingTimeout: 5000,	
});
server.listen(port);

// load in our classes
var User = require('./user');
var Game = require('./game');
var Cards = require('./cards');

// callback helper
function cb(callback) {
	var args = [].slice.call(arguments,1);
	if(typeof callback === 'function') { callback.apply(this, args); }
}


// listen for connections and respond to events
io.sockets.on('connection', function (socket) {
	console.log('New Connection');
	var user = null;

	// handle disconnction and cleanup
	socket.on('disconnect', function () {
		console.log(user ? user.id : '?', 'DISCONNECT');
		if(user) {
			user.removeSocket(socket);
			user = null;
		}
		socket = null;
	});

	socket.on('login', function (name, userId, key, callback) {
		console.log(user ? user.id : '?', 'LOGIN', name, userId);
		if(typeof key === 'function') {
			callback = key;
			key = null;
		}
		if(typeof userId === 'function') {
			callback = userId;
			userId = null;
		}
		if(typeof name !== 'string') { return cb(callback, 'Bad args'); }
		name = name.substr(0,13).replace(/^\s+/,'').replace(/\s+$/,'');
		if(userId && typeof userId !== 'string') { return cb(callback, 'Bad args'); }
		if(key && typeof key !== 'string') { return cb(callback, 'Bad args'); }

		console.log('Login attempt '+name+' ('+userId+')');
		var foundUser = User.users[userId];
		if(foundUser) {
			console.log('User found');
			if(foundUser.key != key) {
				return cb(callback, 'Invalid login key');
			}
			// check that a name change doesn't conflict with another user
			if(foundUser.name !== name) {
				for(var auserId in User.users) {
					if(User.users[auserId].name === name) {
						console.log('Name already in use');
						return cb(callback, 'Name already in use');
					}
				}
				foundUser.setName(name);
			}
			// remove this socket from old user and add to new
			if(user !== foundUser) {
				if(user) {
					user.removeSocket(socket);
				}
				user = foundUser;
				user.addSocket(socket);
			}
		} else {
			// check that the requested name doesn't conflict
			for(var auserId in User.users) {
				if(User.users[auserId].name === name) {
					console.log('Name already in use');
					return cb(callback, 'Name already in use');
				}
			}
			// create new user
			console.log('new user');
			user = new User(name, socket);
		}


		console.log('Login from '+user.name+' ('+user.id+')');
		cb(callback, null, user.id, user.game ? user.game.id : null, user.key);
		if(user.game) {
			user.game.refreshUser(user);
		}
	});

	socket.on('list', function (callback) {
		console.log(user ? user.id : '?', 'LIST');
		var gameList = {};
		for(var gameId in Game.games) {
			var game = Game.games[gameId];
			gameList[game.id] = {
				name: game.name,
				scores: game.getScores(),
				rules: game.rules
			};
		}
		cb(callback, null, gameList);
	});

	socket.on('lusers', function (callback) {
		console.log(user ? user.id : '?', 'LUSERS');
		var userList = {};
		for(var userId in User.users) {
			var luser = User.users[userId];
			userList[luser.id] = {
				name: luser.name,
				gameId: luser.game ? luser.game.id : null
			};
		}
		cb(callback, null, userList);
	});

	socket.on('create', function (name, rules, callback) {
		console.log(user ? user.id : '?', 'CREATE', name);
		if(typeof name !== 'string' || name.length < 1) { return cb(callback, 'Bad args'); }
		name = name.substr(0,100);

		for(var gameId in Game.games) {
			if(Game.games[gameId].name === name) {
				return cb(callback, 'Game name already in use');
			}
		}

		if(!user) { return cb(callback, 'Not logged in'); }
		if(user.game) {
			user.game.removeUser(user);
		}

		var game = new Game(name, rules);
		game.addUser(user);
		console.log('User '+user.name+' created game: '+game.name+' ('+game.id+')');
		cb(callback, null, game.id);
	});

	socket.on('join', function (gameId, callback) {
		console.log(user ? user.id : '?', 'JOIN', gameId);
		if(typeof gameId !== 'string') { return cb(callback, 'Bad args'); }

		if(!user) {
			cb(callback,'no user');
			return;
		}
		if(!Game.games[gameId]){
			cb(callback,callback,'game not found');
			return;
		}
		if(Game.games[gameId] === user.game){
			cb(callback,callback,'User already in game.');
			return;
		}
		if(user.game) {
			user.game.removeUser(user);
		}
		var game = Game.games[gameId];
		game.addUser(user);
		console.log('User '+user.name+' joined game: '+game.name+' ('+game.id+')');
		cb(callback, null, game.id, game.name);
	});

	socket.on('part', function (callback) {
		console.log(user ? user.id : '?', 'PART');
		if(!user) { return cb(callback, 'Not logged in'); }
		if(!user.game) { return cb(callback, 'No current game'); }
		user.game.removeUser(user);
		cb(callback, null);
	});


	socket.on('kick', function (userId, callback) {
		console.log(user ? user.id : '?', 'KICK');
		if(typeof userId !== 'string') { return cb(callback, 'Bad args'); }

		if(!user) { return cb(callback, 'Not logged in'); }
		if(!user.game) { return cb(callback, 'No current game'); }
		var kickUser = user.game.getUSerById(userId);
		if(!kickUser) { return cb(callback, 'User not found'); }
		user.game.removeUser(kickUser);
		cb(callback, null);
	});

	socket.on('swapcards', function (callback) {
		console.log(user ? user.id : '?', 'SWAPCARDS');
		if(!user) { return cb(callback, 'Not logged in'); }
		if(!user.game) { return cb(callback, 'No current game'); }
		if(user.game.state !== 'playcards') {
			return cb(callback, 'Users not playing cards'); }
		var error = user.game.swapCards(user);
		cb(callback, error);
	});


	socket.on('start', function (callback) {
		console.log(user ? user.id : '?', 'START');
		if(!user) { return cb(callback, 'Not logged in'); }
		if(!user.game) { return cb(callback, 'No current game'); }
		if(user.game.state !== 'created' && user.game.state !== 'gameover') {
			return cb(callback, 'Game already started'); }
		if(user.game.users.length < user.game.rules.minimumPlayers) { return cb(callback, 'Not enough players'); }
		user.game.start();
		console.log('User '+user.name+' started game: '+user.game.name+' ('+user.game.id+')');
		cb(callback, null, user.game.id);
	});

	socket.on('play', function (cardIdArray, callback) {
		console.log(user ? user.id : '?', 'PLAY', cardIdArray);
		if(typeof cardIdArray !== 'object' || !cardIdArray.splice) { return cb(callback, 'Bad args'); }
		if(!user) { return cb(callback, 'Not logged in'); }
		if(!user.game) { return cb(callback, 'No current game'); }
		if(!user.game.userCanPlay(user)) { return cb(callback, 'You cannot play your cards now'); }
		if(cardIdArray.length !== user.game.answersRequired()) {
			return cb(callback, 'Wrong number of cards');
		}
		user.play(cardIdArray);
		cb(callback, null);
	});

	socket.on('best', function (userId, callback) {
		console.log(user ? user.id : '?', 'BEST', userId);
		if(typeof userId !== 'string') { return cb(callback, 'Bad args'); }
		if(!user) { return cb(callback, 'Not logged in'); }
		if(!user.game) { return cb(callback, 'No current game'); }
		if(!user.isCzar()) { return cb(callback, 'You are not the Czar'); }
		var bestUser = user.game.getUserById(userId);
		if(!bestUser) { return cb(callback, 'User not found'); }
		if(!user.game.userCanBeBest(bestUser)) { return cb(callback, 'Invalid best user'); }
		user.game.best(bestUser);
		cb(callback, null);
	});

	socket.on('end', function (callback) {
		console.log(user ? user.id : '?', 'END');
		if(!user) { return cb(callback, 'Not logged in'); }
		if(!user.game) { return cb(callback, 'No current game'); }
		if(user.game.currentCzar !== user) { return cb(callback, 'You are not the Czar'); }
		user.game.gameOver();
		cb(callback, null);
	});

	socket.on('chat', function (text, callback) {
		console.log(user ? user.id : '?', 'CHAT');
		if(typeof text !== 'string' || text.length < 1 || text.length > 200) { return cb(callback, 'Bad args'); }
		if(!user) { return cb(callback, 'Not logged in'); }
		if(!user.game) { return cb(callback, 'No current game'); }
		user.game.chat(user, text);
		cb(callback, null);
	});


	setTimeout(function(){
		socket.emit('identify', packagejson.version, Cards.packFullNames, Game.defaultRules);
	}, 100);

});



