
function sentence(str) {
	if (typeof str !== 'string') return str;
	str = str.substr(0,1).toUpperCase() + str.substr(1);
	if (/[^.?!]$/.test(str)) str += '.';
	return str;
}

function CAH() {
	var cah = this;
	var serverVersion = null;

	document.title = CAH.settings.name;
	$('#handtext').text(CAH.settings.name);

	window.s = cah.socket = io.connect(CAH.settings.endPoint);

	function emit() {
		var args = [].slice.apply(arguments);
		if(CAH.settings.debug) {
		    var logargs = args.slice();
		    if(typeof args[args.length-1] === 'function') {
				logargs[args.length-1] = 'cb()';
		    	var cb = args.pop();
			    args.push(function(){
				    var cbargs = [].slice.call(arguments);
					console.log("Callback:", cbargs);
				    cb.apply(this, cbargs);
			    });
		    }
			console.log("Emit:", logargs);
		}
		return cah.socket.emit.apply(cah.socket, args);
	}

	cah.socket.on('error', function (error) {
		console.error(error);
	});

	cah.socket.on('connecting', function () {
		console.log("Connecting");
		cah.log.info('Connecting...');
	});
	cah.socket.on('connect', function () {
		console.log("Connected");
		cah.log.info('Connected');
	});
	cah.socket.on('disconnect', function () {
		console.log("Disconnected");
		cah.log.info('Disconnected');
	});

	cah.socket.on('pingpong', function(stamp, cb) {
		console.log('PING');
		cb(stamp);
	});

	cah.countDown = function(timeout){
		timeout = Math.ceil(timeout) || 0;
		console.log('COUNTDOWN', timeout);
		if(cah.countDown.timer) {
			clearTimeout(cah.countDown.timer);
			cah.countDown.timer = null;
		}
		if(timeout > 0) {
			cah.countDown.timer = setTimeout(function(){
				cah.countDown(timeout - 1);
			},1000);
			$('#countDown').text('Count down: '+timeout);
			if(timeout <= 5) {
				$('#countDown').addClass('warning');
			} else {
				$('#countDown').removeClass('warning');
			}
		} else {
			$('#countDown').text('').removeClass('warning');
		}
	};
	cah.countDown.timer = null;


	cah.updateState = function(state){
		if(state){ cah.state = state; }
		console.log('State:',cah.state);

		if(cah.state === 'created' || cah.state === 'gameover') {
			$('#startButton').show().addClass('enabled');;
			$('#countDown').hide();
			cah.countDown(0);
		} else {
			$('#startButton').hide();
			$('#countDown').show();
		}

		if(cah.state === 'played') {
			cah.countDown(0);
		}

		if(cah.state === 'playcards') {
			$('#swapButton').addClass('enabled');
		} else {
			$('#swapButton').removeClass('enabled');
		}

		if(cah.gameId) {
			$('#partButton').addClass('enabled');
		} else {
			$('#partButton').removeClass('enabled');
		}

		// clear hand cards on these state changes
		if(state === 'created' || state === 'choosegame') {
			$('#hand').children().fadeOut('400',function(){
				$('#hand').empty();
			});
			cah.cards = [];
			cah.myPlay = [];
		}
		if(cah.state === 'playcards' && cah.userId !== cah.czarId) {
			$('#hand').removeClass('disabled');
		} else {
			$('#hand').addClass('disabled');
		}
		if(cah.state === 'playcards' && cah.userId !== cah.czarId) {
			$('#hand').addClass('selectable');
		} else {
			$('#hand').removeClass('selectable');
		}

		// clear table on these state changes
		if(state === 'created' || state === 'watching' || state === 'playcards' || state === 'choosegame') {
			$('#table .content .play').fadeOut('400',function(){
				$('#table .content').empty();
			});
			cah.played = {};
			cah.myPlay = [];
		}
		if(cah.state === 'choosebest' && cah.userId === cah.czarId) {
			$('#table').addClass('selectable');
		} else {
			$('#table').removeClass('selectable');
		}

		// reset countdown on these state changes
		if(state === 'bestchosen' || state === 'gameover' || state === 'started' || state === '') {
			cah.countDown(0);
		}

		// clear question on these state changes
		if(state === 'created' || state === 'choosegame') {
			$('#question').children().fadeOut('400',function(){
				$('#question').empty();
			});
			cah.questionCard = null;
		}

		// update scores display
		$('#chatWindow .scores').empty();
		for(var userId in cah.scores) {
			var line = $(scoreLine).clone().attr('id','');
			if(userId === cah.czarId) line.addClass('czar');
			if(cah.scores[userId].spectator) line.addClass('spectator');
			line.find('.name').text(cah.scores[userId].name);
			line.find('.score').text(cah.scores[userId].score);
			$('#chatWindow .scores').append(line);
		}


		if(state === 'choosename') {
			cah.showNameDialog();
		}

		if(state === 'choosegame') {
			cah.showGameListDialog();
		}

		if(state === 'newgame') {
			cah.showNewGameDialog();
		}

		$('#handtext').text(cah.gameId ? cah.gameName : CAH.settings.name);

	};

	$('#startButton').click(function(){
		if(cah.state === 'created' || cah.state === 'gameover') {
			emit('start', function(error){
				if(error) { return cah.log.error(error); }
			});
		}
	});
	$('#partButton').click(function(){
		if(cah.gameId && confirm('Are you sure you want to leave the game?')) {
			emit('part', function(error){
				if(error) { return cah.log.error(error); }
			});
		}
	});

	$('#swapButton').click(function(){
		if(cah.state === 'playcards' && confirm('Are you sure you want to swap your cards for one point?')) {
			emit('swapcards', function(error){
				if(error) { return cah.log.error(error); }
			});
		}
	});

	// load user
	cah.userName = localStorage.getItem('userName');
	cah.userId = sessionStorage.getItem('userId');

	cah.cardPacks = [];
	cah.defaultRules = [];
	cah.socket.on('identify',function(version,cardPacks,defaultRules){
		cah.cardPacks = cardPacks;
		cah.defaultRules = defaultRules;
		// version check
		if(version && serverVersion && version !== serverVersion) {
			return location.reload(true);
		}
		serverVersion = version;

		$('#serverVersion').text(serverVersion);

		// login if we think we should be logged in
		if(cah.userName && cah.userId) {
			cah.login();
		} else {

			// show login screen
			cah.updateState('choosename');

		}
	});

	// chat window
	cah.speak = function(text) {
		if (!window.speechSynthesis) return;
		var msg = new SpeechSynthesisUtterance();
		if (!cah.voice) {
			cah.voice = speechSynthesis.getVoices().filter(function(voice){
				return voice.lang.startsWith('en');
			})[0];
		}
		msg.voice = cah.voice;
		msg.text = text.replace('/_/g', ' blank ');
		window.speechSynthesis.speak(msg);
	};
	cah.log = function(tag, text, aclass, speak) {
		var content = $('#chatWindow .chat .content');
		var line = $('#chatLine').clone().attr('id', '');
		if(aclass) {
			line.addClass(aclass);
		}
		line.find('.name')
			.text(tag);
		line.find('.text')
			.text(text);
		line
			.appendTo(content);
		// scroll to bottom
		setTimeout(function(){
			var wrapper = $('#chatWindow .chat');
			var height = content.height();
			wrapper.animate({ scrollTop: height }, 100);
		});

		if (speak) cah.speak(text);

		return cah;
	};
	cah.log.error = function(error, speak) {
		cah.log('Error:',error,'error', speak);
		return false;
	};
	cah.log.info = function(info, speak) {
		cah.log('',info,'info', speak);
		return cah;
	};
	cah.log.game = function(info, speak) {
		cah.log('',info,'game');
		$('#table .info').text(info, speak);
		return cah;
	};
	cah.socket.on('chat', function(userId, text) {
		cah.log(cah.getUserName(userId)+':',text, userId === cah.userId ? 'mychat':'otherchat');
	});

	// listen for key presses
	$(document.body).keydown(function(event) {
		if(!$(document.body).hasClass('modal')) {
			var code = (event.keyCode ? event.keyCode : event.which);
			if(code == 13) {
				event.preventDefault();
				var text = $('#chatWindowInput').val();
				if(text) {
		 			emit('chat', text);
		 		}
				$('#chatWindowInput').val('');
			} else {
				$('#chatWindowInput').focus();
			}
		}
	});

	// cards in hand
	cah.cards = [];
	cah.myPlay = [];
	cah.addCard = function(newCard) {
		var card = new CAH.Card('white', newCard, true);
		cah.cards.push(card);
		card.appendTo('#hand');
		card.element.click(function(){
			if(cah.czarId !== cah.userId && cah.state === 'playcards') {
				// toggle
				var alreadyClicked = cah.myPlay.indexOf(card);
				if(alreadyClicked >= 0) {
					cah.myPlay.splice(alreadyClicked, 1);
					card.element.removeClass('expand');
				} else {
					cah.myPlay.push(card);
					card.element.addClass('expand');
				}

				if(cah.myPlay.length == cah.questionCard.card.answersRequired) {
					cah.updateState('played');
					var cardIdArray = [];
					cah.myPlay.forEach(function(card){
						cardIdArray.push(card.card.id);
					});
					cah.log.game('Waiting for the other players to pick their cards.');
					emit('play',cardIdArray, function(error){
						if(error) {
							cah.updateState('playcards');
							cah.myPlay.forEach(function(card) {
								card.element.removeClass('expand');
							});
							cah.myPlay = [];
							return cah.log.error(error);
						}
						cah.myPlay.forEach(function(card){
							cah.removeCard(card);
						});

					});
				}
			}
		});

	};
	cah.removeCard = function(card, imediate) {
		var pos = cah.cards.indexOf(card);
		if(pos < 0){ return; }
		cah.cards.splice(pos,1);
		if(imediate) {
			card.element.remove();
		} else {
			card.element.fadeOut(400, function(){
				card.element.remove();
			});
		}
	};
	cah.socket.on('cards', function(cards) {
		for(var i = cah.cards.length-1; i>=0; i--) {
			if(!card[ cah.cards[i].card.id ]) {
				// delete this card
				cah.removeCard(cah.cards[i], true);
			} else {
				// we already have this card
				delete card[ cah.cards[i].card.id ];
			}
		}
		// add the remainding cards
		for(var id in cards) {
			cah.addCard(cards[id]);
		}
	});
	cah.socket.on('card', function(card) {
		cah.addCard(card);
	});

	// scores / users
	cah.scores = {};
	cah.socket.on('scores', function(scores) {
		cah.scores = scores;
		cah.updateState();
	});
	cah.getUserName = function(userId){
		var user = cah.scores[userId];
		return user ? user.name : '';
	};

	cah.gameId = null;
	cah.gameName = null;

	cah.socket.on('game', function(gameId, gameName) {
		cah.gameId = gameId;
		cah.gameName = gameName;
		// display game name
		cah.log.info('Game name: '+cah.gameName);
		cah.updateState();
	});

	cah.socket.on('parted', function(gameId, gameName) {
		cah.log.info('You have left the game.');
		cah.gameId = null;
		cah.gameName = null;
		cah.updateState('choosegame');
	});

	cah.socket.on('besttimeout', function() {
		if(cah.czarId === cah.userId) {
			cah.log.game('You took too long.');
		} else {
			cah.log.game('The Czar took too long.');
		}
		cah.updateState('bestchosen');
	});

	cah.socket.on('join', function(userId) {
		cah.log.info(cah.getUserName(userId)+' has joined the game.');
	});
	cah.socket.on('part', function(userId) {
		cah.log.info(cah.getUserName(userId)+' has left the game.');
		// remove any cards they have on the table
		if(cah.played[userId]) {
			cah.played[userId].forEach(function(card){
				card.destroy();
			});
			delete cah.played[userId];
		}
	});

	cah.czarId = null;
	cah.socket.on('czar', function(czarId) {
		// reset old czar
		// show who is the current czar
		cah.czarId = czarId;
		if(cah.userId === cah.czarId) {
			cah.log.game('You are the Czar.');
		} else {
			cah.log.game(cah.getUserName(cah.czarId)+' is the Czar.');
		}
		cah.updateState();
	});

	cah.questionCard  = null;
	cah.socket.on('question', function(card, inPlay) {
		if(!card) {
			if(cah.questionCard) { cah.questionCard.destroy(); }
			cah.questionCard = null;
			cah.updateState('created');
			return;
		} else {
			var oldCard = cah.questionCard;
			cah.questionCard = new CAH.Card('black', card, true);

			// hide old card and show new card
			var showNew = function() {
				$('#question').empty();
				cah.questionCard.appendTo('#question');
				$('#question').append('<div class="czar"></div>');
				$('#question .czar').text('Czar: '+cah.getUserName(cah.czarId));
			};
			if(oldCard) {
				oldCard.destroy(showNew);
			} else {
				showNew();
			}

			if(cah.userId === cah.czarId) {
				cah.log.game('Waiting for players to pick their cards.');
			} else if(!inPlay) {
				cah.log.game('Watching the game.');
			} else {
				cah.speak(cah.questionCard.card.text);
				if(cah.questionCard.card.answersRequired < 2) {
					cah.log.game('Pick a card.', true);
				} else {
					cah.log.game('Pick '+cah.questionCard.card.answersRequired+' cards.', true);
				}
			}

			if(inPlay) {
				cah.updateState('playcards');
			} else {
				cah.updateState('watching');
			}
		}
	});

	cah.played = {};
	cah.socket.on('played', function(played, timeout) {
		played.forEach(function(userId){
			if(!cah.played[userId]) {
				var clickfn;
				if(cah.userId === cah.czarId) {
					clickfn = function(){
						if(cah.state === 'choosebest' && cah.userId === cah.czarId) {
							emit('best',userId, function(error){
								if(error) {
									return cah.log.error(error);
								}
							});
						}
					};
				}
				cards = [];
				var play = $('<a class="play"></a>');
				$('#table .content').append(play);
				for(var i = 0; i<cah.questionCard.card.answersRequired; i++) {
					var card = new CAH.Card('white');
					cards.push(card);
					card.appendTo(play);
					if(cah.userId === cah.czarId) {
						card.element.click(clickfn);
					}
				}
				cah.played[userId] = cards;
			}

		});
		cah.countDown(timeout);
	});
	cah.socket.on('reveal', function(played, timeout) {
		if(!cah.questionCard) {
			return console.error('Reveal but no question card');
		}

		// populate and turnover
		for(var userId in played) {
			for(var i = 0; i<cah.played[userId].length; i++) {
				var card = cah.played[userId][i];
				card.set(played[userId][i]);
				card.show();
			}
		}

		if(cah.userId === cah.czarId) {
			cah.speak(cah.questionCard.card.text);
			cah.log.game('Pick the best answer.', true);
			cah.countDown(timeout);
		} else {
			cah.log.game(cah.getUserName(cah.czarId)+' is picking the best card'+(cah.questionCard.card.answersRequired>1?'s':'')+'.');
		}

		cah.updateState('choosebest');
	});

	cah.socket.on('dropped', function(userId) {
		// populate and turnover
		if(cah.played[userId]) {
			for(var i = 0; i<cah.played[userId].length; i++) {
				var card = cah.played[userId][i];
				card.destroy();
			}
		}

		if(userId === cah.userId) {
			cah.log.game('You have been dropped from this go for taking too long.');
			cah.myPlay.forEach(function(card) {
				card.element.removeClass('expand');
			});
			cah.updateState('played');
		} else {
			cah.log.info(cah.getUserName(userId)+' has been dropped from this go for taking too long.');
		}

	});

	cah.socket.on('spectator', function(userId) {
		if(userId === cah.userId) {
			cah.log.info('You have become a spectator.');
		} else {
			cah.log.info(cah.getUserName(userId)+' has become a spectator.');
		}
	});

	cah.socket.on('swapped', function(userId) {
		cah.log.info(cah.getUserName(userId)+' has swapped their cards.');
	});

	cah.socket.on('best', function(userId) {
		if(!cah.questionCard) {
			return console.error('Reveal but no question card');
		}

		if(cah.played[userId]) {
			cah.played[userId][0].element.parent().addClass('expand');
		}

		cah.log.game(cah.getUserName(userId)+' had the best card'+(cah.questionCard.card.answersRequired>1?'s':'')+'.', true);

		var bestCards = cah.played[userId].slice();
		var bestLine = cah.questionCard.card.text;
		while(bestCards.length && bestLine.match(/_+/)) {
			bestLine = bestLine.replace(/_+/,
				bestCards.shift().card.text
					.replace(/\.$/,'')
					.replace(/^A /, 'a ')
					.replace(/^The ([a-z])/, 'the $1')
			);
		}

		// convert html to text
		bestLine = $('<span>').html(bestLine).text();

		cah.log.info('"'+bestLine+'"', true);


		// cards left after replacements
		if(bestCards.length) {
			var bestAnswer = '';
			while(bestCards.length) {
				var answer = bestCards.shift().card.text;
				// not last card
				if(bestCards.length) answer = answer.replace(/\.$/,'');
				// adding to an existing answer
				if(bestAnswer.length) {
					if (last) bestAnswer += ' and ';
					else bestAnswer += ', ';
				}
				bestAnswer += answer;
			}

			bestAnswer = sentence(bestAnswer);

			// convert html to text
			bestAnswer = $('<span>').html(bestAnswer).text();

			cah.log.info('"'+bestAnswer+'"');
		}

		cah.updateState('bestchosen');
	});

	cah.socket.on('gameover', function() {
		if(cah.questionCard) { cah.questionCard.destroy(); }
		cah.questionCard = null;
		cah.updateState('gameover');

		var users = [];
		var score = 1;
		for(var id in cah.scores) {
			if(cah.scores[id].score > score) {
				score = cah.scores[id].score;
				users = [cah.scores[id].name];
			} else if(cah.scores[id].score === score) {
				users.push(cah.scores[id].name);
			}
		}

		if(users.length === 0) {
			cah.log.game('Game aborted!');
		} else if(users.length === 1) {
			cah.log.game('Game Over! '+users[0]+' has won.');
		} else {
			cah.log.game('Game Over! '+users.slice(0,-1).join(', ')+' and '+users.slice(-1)+' have drawn.');
		}
	});


	cah.showDialog = function(dialog) {
		$(document.body).addClass('modal');
		$(dialog).fadeIn(200);
	};
	cah.hideDialog = function(dialog) {
		$(dialog).hide();
		$(document.body).removeClass('modal');
	};


	cah.showNameDialog = function() {
		$('#loginButton').addClass('enabled');
		$('#loginName').val(cah.userName ? cah.userName : '');

		cah.hideDialog('#games.dialog');
		cah.hideDialog('#create.dialog');
		cah.showDialog('#login.dialog');

		$('#loginName').focus();
	};
	$('#loginName').keydown(function(event) {
		var code = (event.keyCode ? event.keyCode : event.which);
		if(code == 13) {
			event.preventDefault();
			$('#loginButton').click();
		}
	});
	$('#loginButton').click(function(){
		if(cah.state !== 'choosename'){ return; }
		var name = $('#loginName').val();
		if(!name.length) { return; }
		cah.login(name);
	});

	cah.login = function(name) {
		$('#loginButton').removeClass('enabled');
		cah.updateState('loggingin');

		var key = sessionStorage.getItem('userKey');
		if(key === "null") { key = null; }

		emit('login', name || cah.userName, cah.userId, key, function(error, userId, gameId, key) {
			if(error) {
				cah.updateState('choosename');
				alert(error);
				cah.log.error(error);
				$('#loginButton').addClass('enabled');
				return;
			}
			localStorage.setItem('userName', cah.userName);
			sessionStorage.setItem('userId', cah.userId = userId);
			sessionStorage.setItem('userKey', key);
			cah.gameId = gameId;

			cah.hideDialog('#login.dialog');

			if(!cah.gameId) {
				cah.updateState('choosegame');
			}
		});
	};

	cah.showGameListDialog = function(){
		$('#gameList').empty();
		emit('list',function(err,games){
			$('#gameList').empty();
			for(var gameId in games){
				(function(gameId){
					var game = $('<a class="game"></a>');
					var players = Object.keys(games[gameId].scores).length;
					game.text(games[gameId].name+' ('+players+' player' + (players === 1 ? '' : 's')+ ')');
					game.click(function(){
						cah.speak('Joining game: ' + games[gameId].name);
						emit('join',gameId, function(error, gameId) {
							if(error) {
								cah.updateState('choosegame');
								alert(error);
								return cah.log.error(error);
							}
							cah.gameId = gameId;
							cah.hideDialog('#games.dialog');
						});
					});
					$('#gameList').append(game);
				})(gameId);
			}
			if(Object.keys(games).length === 0) {
				$('#gameList').append('<div class="nogames">No games on this server. Create a new one.</div>');
			}
		});
		$('#newGameButton').addClass('enabled');
		$('#newGameButton').click(function(){
			if(cah.state !== 'choosegame'){ return; }
			cah.hideDialog('#games.dialog');
			cah.updateState('newgame');
		});
		$('#refreshGamesButton').addClass('enabled');
		$('#refreshGamesButton').click(function(){
			if(cah.state !== 'choosegame'){ return; }
			cah.updateState('choosegame');
		});
		$('#loginAgainButton').addClass('enabled');
		$('#loginAgainButton').click(function(){
			if(cah.state !== 'choosegame'){ return; }
			cah.updateState('choosename');
		});
		cah.hideDialog('#create.dialog');
		cah.hideDialog('#login.dialog');
		cah.showDialog('#games.dialog');
	};


	cah.showNewGameDialog = function() {
		$('#createGameName').val('');
		$('#createGameButton').addClass('enabled');
		$('#cancelGameButton').addClass('enabled');
		$('#cardPacks').empty();
		cah.cardPacks.forEach(function(cardPack){
			$('#cardPacks')
				.append(
					$('<input type="checkbox">')
                    .attr('checked', true)
                    .attr('id','cardPack'+cardPack.name))
				.append($('<label>')
					.attr('for','cardPack'+cardPack.name)
					.text('Card Pack: '+cardPack.fullName));
		});
		cah.hideDialog('#games.dialog');
		cah.showDialog('#create.dialog');
		$('#createGameName').focus();
	};
	$('#createGameName').keydown(function(event) {
		var code = (event.keyCode ? event.keyCode : event.which);
		if(code == 13) {
			event.preventDefault();
			$('#createGameButton').click();
		}
	});
	$('#cancelGameButton').click(function(){
		if(cah.state !== 'newgame'){ return; }
		cah.updateState('choosegame');
	});
	$('#createGameButton').click(function(){
		if(cah.state !== 'newgame'){ return; }
		var name = $('#createGameName').val();
		if(!name.length) { return; }

		var rules = {
			minimumPlayers: $('#rulesMinPlayers').val(),
			maximumPlayers: $('#rulesMaxPlayers').val(),
			playerTimeout: $('#rulesPlayerTimeout').val(),
			bestTimeout: $('#rulesBestTimeout').val()
		};
		rules.cardPacks = [];
		cah.cardPacks.forEach(function(cardPack){
			if($('#cardPack'+cardPack.name).is(':checked')) {
				rules.cardPacks.push(cardPack.name);
			}
		});
		if($('#rulesTimeout').is(':checked')) {
			rules.timeout = 30000;
		}

		$('#createGameButton').removeClass('enabled');
		cah.updateState('creatinggame');

		cah.speak('Creating game: ' + name);

		emit('create', name, rules, function(error, gameId) {
			if(error) {
				cah.updateState('newgame');
				alert(error);
				return cah.log.error(error);
			}
			cah.gameId = gameId;
			cah.hideDialog('#create.dialog');
		});

	});


	cah.updateState('connecting');
}

CAH.Card = function(type, card, hidden) {
	this.type = type;
	this.card = null;
	this.element = $('#card').clone();
	this.element.attr('id', '');
	this.element.find('.back').text(CAH.settings.name);
	this.element.find('.logo').text(CAH.settings.name);
	if(hidden || !card) {
		this.element.addClass('hidden');
	}
	this.element.addClass(type === 'black' ? 'black' : 'white');
	if(card) { this.set(card); }
};
CAH.Card.prototype.set = function(card) {
	this.card = card;
	if(!this.card) { return; }
	var text = this.card.text;
	if (this.type === 'white') text = sentence(text);
	this.element.find('.text').html(text);
	if(this.type !== 'black' || this.card.answersRequired != 2) {
		this.element.find('.pick2').hide();
	} else {
		this.element.find('.pick2').show();
	}
};
CAH.Card.prototype.appendTo = function(el, callback) {
	var that = this;
	this.element.appendTo(el);
	var showMe = !!this.card;
	this.element.hide().fadeIn(400, function(){
		if(showMe) { that.show(); }
		if(typeof callback === 'function') { callback(); }
	});
};
CAH.Card.prototype.show = function() {
	if(!this.card) { return; }
	this.element.removeClass('hidden');
};
CAH.Card.prototype.hide = function() {
	this.element.addClass('hidden');
};
CAH.Card.prototype.destroy = function(callback) {
	var that = this;
	this.element.fadeOut(400, function(){
		that.element.remove();
		if(typeof callback === 'function') { callback(); }
	});
};


CAH.settings = {
	name: 'Hards Against Cumanity',
	debug: true
};


$(document).ready(function(){ new CAH(); });


